package com.sandkasten.simulation;

public class Point<T extends Number> {
  public static final Point<Integer> ORIGIN = new Point<Integer>(0, 0);

  public final T x;
  public final T y;

  public Point(T x, T y) {
    this.x = x;
    this.y = y;
  }

  @Override
  public String toString() {
    return "{Point: [x: " + x + ", y: " + y + "]}";
  }
}