package com.sandkasten.simulation;

import java.awt.Color;

import processing.core.PApplet;

import com.sandkasten.simulation.model.*;
import com.sandkasten.simulation.model.Terrain.Topography;

public class Game extends PApplet {
  public static void main(String[] args) {
    PApplet.main(Game.class.getName());
  }

  public void settings() {
    size(800, 600);
  }

  /**
   * The amount of VERTICAL lines.
   * 
   * @see {@link Game.xRows}
   */
  private int yColumns;

  /**
   * The amount of HORIZONTAL lines.
   * 
   * 
   * Sketch:
   * 
   * <p>
   * 0.Row __|__|__|__|__|__|__|__|__
   * </p>
   * <p>
   * 1.Row __|__|__|__|__|__|__|__|__
   * </p>
   * <p>
   * 2.Row __|__|__|__|__|__|__|__|__
   * </p>
   * <p>
   * 3.Row __|__|__|__|__|__|__|__|__
   * </p>
   * 
   * @see {@link Game.yColumns}
   */
  private int xRows;

  private Grid grid;

  Terrain t;
  Item ig;
  public void setup() {
    translate(100, 100);
    yColumns = 8;
    xRows = 5;
    t=new Terrain(Topography.grass);
    ig=new Item(new Color(200));
    grid = new Grid(yColumns, xRows);
    //t.show(this, grid, new Point<Integer> (3,1));
    // fill grid
   
    for(int i=0;i<10;i++){
      for(int j=0;j<10;j++){
        t.show(this, grid, new Point<Integer>(i, j));
      }
    }
    for(int i=0;i<22;i++){
      for(int j=0;j<11;j++){
        ig.show(this, grid, new Point<Integer>(i, j));
      }
    }
    

  }

  public void draw() {
    //background(255);
    // grid.show(this, Point.ORIGIN);
    //for (int i = 0; i < 8; i++) {
      //for (int j = 0; j < 7; j++) {
        //Point<Integer> p = new Point<Integer>(i, j);
        //println(grid.getItem(p));
        //println(p);
        //println(grid.getHexagons());
        //println(grid.getItems());
        
        //grid.getTerrain(p).show(this, grid, p);
      //}
    //}
    // noFill();
    // t.show(this, grid, new Point(5, 5));
  }

  public void mouseDragged() {
  }

  public void mouseClicked() {
  }

  public void keyPressed() {
  }
}