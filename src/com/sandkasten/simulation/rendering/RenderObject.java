package com.sandkasten.simulation.rendering;

import processing.core.PApplet;

import com.sandkasten.simulation.Point;

abstract public class RenderObject {
  abstract public void show(PApplet sketch, Point<Integer> point);
}
