package com.sandkasten.simulation.rendering;

import processing.core.PApplet;

import com.sandkasten.simulation.Grid;
import com.sandkasten.simulation.Point;

abstract public class RenderGridObject {
  abstract public void show(PApplet sketch, Grid grid, Point<Integer> point);

  abstract protected Point<Float> translateToPixelPoint(Point<Integer> point, float factor);
}