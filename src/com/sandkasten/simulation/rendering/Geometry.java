package com.sandkasten.simulation.rendering;

import com.sandkasten.simulation.Point;


import processing.core.PApplet;
import processing.core.PConstants;

public class Geometry {

    public static void hexagon(PApplet sketch, Point<Float> point, float unitSize) {
        
        
        sketch.beginShape();
        for (int k = 0; k < 6; k++) {
            float angle_deg = 60 * k - 30;
            float angle_rad = PConstants.PI / 180 * angle_deg;
            sketch.vertex(point.x + unitSize * PApplet.cos(angle_rad), point.y + unitSize * PApplet.sin(angle_rad));
        }
        sketch.endShape(PConstants.CLOSE);
        
    }

}