package com.sandkasten.simulation;

import java.util.HashMap;

import processing.core.PApplet;

import com.sandkasten.simulation.model.*;
import com.sandkasten.simulation.rendering.RenderObject;

public class Grid extends RenderObject {
  private final HashMap<Point<Integer>, Item> items;
  private final HashMap<Point<Integer>, Terrain> hexagons;

  public final int columns;
  public final int rows;
  public final float unitSize;

  public Grid(int columns, int rows) {
    this.columns = columns;
    this.rows = rows;
    unitSize = 20;
    items = new HashMap<Point<Integer>, Item>();
    hexagons = new HashMap<Point<Integer>, Terrain>();
  }

  @Override
  public void show(PApplet sketch, Point<Integer> point) {
    items.forEach((Point<Integer> p, Item i) -> i.show(sketch, this, p));
    hexagons.forEach((Point<Integer> p, Terrain t) -> t.show(sketch, this, p));
  }

  /**
   * Put an item on an intersection of 3 hexagons
   * 
   * @param point The position on the grid
   * @param item  The item that should be shown on point
   */
  public void put(Point<Integer> point, Item item) {
    items.put(point, item);
  }

  /**
   * Put a terrain on a hexagon
   * 
   * @param point   The position on the grid
   * @param terrain The terrain that should be shown on point
   */
  public void put(Point<Integer> point, Terrain terrain) {
    hexagons.put(point, terrain);
  }

  public Item getItem(Point<Integer> point) {
    return items.get(point);
  }

  public Terrain getTerrain(Point<Integer> point) {
    return hexagons.get(point);
  }
  public HashMap<Point<Integer>, Terrain> getHexagons() {
      return hexagons;
  }
  public HashMap<Point<Integer>, Item> getItems() {
      return items;
  }
}