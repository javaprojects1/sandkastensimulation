package com.sandkasten.simulation.model;

import processing.core.PApplet;

import com.sandkasten.simulation.Grid;
import com.sandkasten.simulation.Point;
import com.sandkasten.simulation.rendering.Geometry;
import com.sandkasten.simulation.rendering.RenderGridObject;

public class Terrain extends RenderGridObject {
  private Topography topography;

  public Terrain(Topography topography) {
    this.topography = topography;
  }

  @Override
  public void show(PApplet sketch, Grid grid, Point<Integer> point) {
    if(topography==Topography.grass){
      sketch.fill(0,255,0);
    }
    if(topography==Topography.rock){
      sketch.fill(200);
    }
    if(topography==Topography.water){
      sketch.fill(0,0,255);
    }
    
    Geometry.hexagon(sketch, translateToPixelPoint(point, grid.unitSize), grid.unitSize);
  }

  public enum Topography {
    grass, rock, water
  }

  @Override
  protected Point<Float> translateToPixelPoint(Point<Integer> point, float factor) {
    /*
    Calculation the realtive Pixel-Cordinates from a List-based-Cordinatensystem
              (source:https://www.redblobgames.com/grids/hexagons/)
    Autor: SmiJ    
    */
    
    float width = (float) (Math.sqrt(3) * factor);              // calc the width of a Hexagon
    

    float x =   point.x * width                                 // calc the x-Coordinat 
              + (point.y % 2) * (width / 2);                    // 2 Cases of 2(y): add a Phaseshift so the Grid is lined up perfectly 

    float y = point.y * 1.5f * factor;                          // calc the x-Coordinat

    return new Point<Float>(x,y);
  }

  

}
