package com.sandkasten.simulation.model;

import java.awt.Color;

import processing.core.PApplet;
import processing.core.PConstants;

import com.sandkasten.simulation.Grid;
import com.sandkasten.simulation.Point;
import com.sandkasten.simulation.rendering.*;

public class Item extends RenderGridObject {

  private Color color;

  public Item(Color color) {
    this.color = color;
  }

  @Override
  public void show(PApplet sketch, Grid grid, Point<Integer> point) {
    sketch.fill(color.getRGB());
    Point<Float> pixelPoint=translateToPixelPoint(point, grid.unitSize);
    sketch.circle(pixelPoint.x, pixelPoint.y, 10);
  }

  public enum Items {
  }

  @Override
  protected Point<Float> translateToPixelPoint(Point<Integer> point, float factor) {
    /*
    Calculation the realtive Pixel-Cordinates from a List-based-Cordinatensystem
              (source:https://www.redblobgames.com/grids/hexagons/)
    Autor: SmiJ    
    */
    float width = (float) (Math.sqrt(3) * factor);                          //  calc width of hexagon
    
    float angle_deg = 150 - 30;
    float angle_rad = PConstants.PI / 180 * angle_deg;                      //  calc the angle for 1 of 2 Cases

    
    float x=  ((point.x+1)%2)*width * PApplet.cos(angle_rad)                // 1 of 2 Cases(x): Calc the X-Cordinate in relation to the center Point
            + ((point.x/2))*width;                                            // new center Point every 2 steps

    float y=  ((point.y+1)%2)*                                              //  1 of 2 Cases(y):
              
                (((point.x+1)%2)*-1*factor/2 * PApplet.sin(angle_rad)           //  calc the height [1 of 2 Cases(x)]
                  + ((point.x)%2)*-1*factor+point.y*1.5f*factor)                  //  calc the height [2 of 2 Cases(x)]
                                                                              
            
            + ((point.y)%2)*                                                 // 2 of 2 Cases(y):
            
              + (((point.x)%2)*-1*factor/2 * PApplet.sin(angle_rad)           //  calc the height [1 of 2 Cases(x)]
                  + ((point.x+1)%2)*-1*factor+point.y*1.5f*factor);                 //  calc the height [2 of 2 Cases(x)]
    
    return new Point<Float>(x,y);
  }

  
}
