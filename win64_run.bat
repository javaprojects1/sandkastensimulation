@echo off
setlocal

set workspaceRoot=%1

:: Set up all the variables needed for the compilation process
set "sourceDir=src"
set "libs=%workspaceRoot%/lib/core.jar"
set "mainWithPackage=com.sandkasten.simulation.Game"

:: The following variables should not be modified by the user
set "binaryDir=%workspaceRoot%/bin"
set "sourceDir=%workspaceRoot%/%sourceDir%"

set mainWithPath=%mainWithPackage:.=/%
set "binaries=%binaryDir%;%libs%"

if not exist "%sourceDir%/" (
  echo %sourceDir%
  goto NO_SRC_DIR
)

:: Check if directory exists, if not create it
if not exist "%binaryDir%/" (
  mkdir %binaryDir%
)

:: Compile everything to .class files
javac -d %binaryDir% -sourcepath %sourceDir% -cp %libs% %sourceDir%/%mainWithPath%.java

:: Execute the main file, that contains the main method
java -cp %binaries% %mainWithPackage%

goto EOF

:NO_SRC_DIR
echo Error!
echo The source directory %sourceDir% does
goto EOF

echo .

:EOF
cmd /k