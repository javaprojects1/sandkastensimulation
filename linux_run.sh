#! /usr/bin/bash

workspaceRoot=$1

# Set up all the variables needed for the compilation process
sourceDir="src"
libs="$workspaceRoot/lib/core.jar"
mainWithPackage="com.sandkasten.simulation.Game"

# The following variables should not be modified by the user
binaryDir="$workspaceRoot/bin"
sourceDir="$workspaceRoot/$sourceDir"

mainWithPath=${mainWithPackage//./"/"}
binaries="$binaryDir:$libs"

# Check if directory exists, if not create it
if !([ -d $binaryDir ]); then
  mkdir $binaryDir
fi

# Compile everything to .class files
javac -d $binaryDir -sourcepath $sourceDir -cp $libs $sourceDir/$mainWithPath.java

# Execute the main file, that contains the main method
java -cp $binaries $mainWithPackage

exit 0
